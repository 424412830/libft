/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/11 19:27:36 by abinet            #+#    #+#             */
/*   Updated: 2022/11/28 17:15:38 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stddef.h>

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	if (n == 0)
		return (0);
	while (*s1 && (n - 1 != 0) && (*s1 == *s2))
	{
		s1++;
		s2++;
		--n;
	}
	return ((unsigned char)*s1 - (unsigned char)*s2);
}
/*

int	main(int argc, char **argv)
{
	(void) argc;
	printf("%d\n", ft_strncmp(argv[1], argv[2], 3));
	return (0);
}
*/
