/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 23:24:14 by abinet            #+#    #+#             */
/*   Updated: 2022/11/10 23:24:16 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <strings.h>

void	ft_bzero(void *s, size_t n)
{
	unsigned int	i;
	char			*str;

	str = s;
	i = 0;
	while (i < n)
	{
		str[i] = '\0';
		i++;
	}
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	int	i;
	
	i = 0;
	
	while (argv[1][i] != '\0')
	{
		printf("%c", argv[1][i]);
		i++;
	}
	printf("\n");
	ft_bzero(argv[1], 8);
	i = 0;
	while (i < 8)
	{
		printf("%c", argv[1][i]);
		i++;
	}
	printf("\n");
	return (0);
}
*/
